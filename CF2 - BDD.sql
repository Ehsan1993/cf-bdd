    
    -- Interrogation des groupes jouant un titre donné:
    
SELECT GRO_NOM, CHA_TITRE
FROM groupe g JOIN repertoire r ON g.GRO_ID = r.GRO_ID
	JOIN chanson c ON r.CHA_ID = c.CHA_ID
    where CHA_TITRE like 'Insane in the Brai';
    
-- Interrogation des rencontres où un titre a été interprété et par qui:

Select REN_NOM, CHA_TITRE, GRO_NOM, PER_NOM, PER_PRENOM
FROM rencontre r JOIN passage p ON r.REN_ID = p.REN_ID
	JOIN groupe g ON g.GRO_ID = p.GRO_ID
    JOIN membre m ON m.GRO_ID = g.GRO_ID
    JOIN personne pe ON pe.PER_ID = m.PER_ID
    JOIN representation re ON re.PAS_ID = p.PAS_ID
    JOIN chanson ch ON ch.CHA_ID = re.CHA_ID
    WHERE CHA_TITRE like 'The End';
    
-- Interrogation des membres ayant une spécialité donnée pour une rencontre donnée:

SELECT PER_NOM, PER_PRENOM, GRO_NOM, SPE_NOM, REN_NOM
FROM membre m JOIN personne p ON p.PER_ID = m.PER_ID
    JOIN groupe g ON g.GRO_ID = m.GRO_ID
	JOIN specialiser s ON s.PER_ID = p.PER_ID
    JOIN specialite sp ON sp.SPE_ID = s.SPE_ID
    JOIN rencontre r ON r.REN_ID = s.REN_ID
    WHERE SPE_NOM like 'soliste' AND REN_NOM like 'CAP FESTIVAL';

-- Interrogation des titres de plus de x minutes pour un pays ou une région donnée:

SELECT CHA_TITRE, CHA_TPS, REG_NOM, PAY_NOM
from chanson ch JOIN representation r ON r.CHA_ID = ch.CHA_ID
	JOIN passage p ON p.PAS_ID = r.PAS_ID
    JOIN groupe g ON g.GRO_ID = p.GRO_ID
    JOIN representer re ON re.GRO_ID = g.GRO_ID
    JOIN region ON region.REG_ID = re.REG_ID
    JOIN pays pa ON pa.PAY_ID = region.PAY_ID
    WHERE CHA_TPS > '00:04:00' AND PAY_NOM like 'Allemagne';
    
-- Interrogation des rencontres ayant eu n groupes participants:

SELECT REN_NOM, count(g.GRO_ID) as Groupes_Participants
FROM rencontre r JOIN passage p ON p.REN_ID = r.REN_ID
	JOIN groupe g ON g.GRO_ID = p.GRO_ID
    GROUP BY r.REN_ID
    HAVING Groupes_Participants = 2;
    
-- Interrogation des rencontres où on a joué d'un instrument donné:

SELECT REN_NOM as 'Noms des rencontres où on a joué un instrument'
FROM rencontre r JOIN passage p ON r.REN_ID = p.REN_ID
	JOIN groupe g ON g.GRO_ID = p.GRO_ID
    JOIN membre m ON m.GRO_ID = g.GRO_ID
    JOIN personne pr ON pr.PER_ID = m.PER_ID
    JOIN jouer j ON j.PER_ID = pr.PER_ID
  --  JOIN instrument i ON i.INS_ID = j.INS_ID
    WHERE Pr.PER_ID IN (SELECT j.PER_ID FROM jouer)
    GROUP BY REN_NOM;
    
-- Planning complet de la rencontre par lieu et groupe:

SELECT REN_NOM, REN_LIEU, REN_DATEDEBUT, REN_DATEFIN, REN_NBPERS, GRO_NOM
FROM rencontre r JOIN passage p ON r.REN_ID = p.REN_ID
	JOIN groupe g ON g.GRO_ID = p.GRO_ID
    GROUP BY REN_LIEU;
    
    
SELECT REN_NOM, REN_LIEU, REN_DATEDEBUT, REN_DATEFIN, REN_NBPERS, GRO_NOM
FROM rencontre r JOIN passage p ON r.REN_ID = p.REN_ID
	JOIN groupe g ON g.GRO_ID = p.GRO_ID
    GROUP BY GRO_NOM;
    
-- Créer les fonctions pour contrôler une date de rencontre

SELECT PAS_DATE, dayname(PAS_DATE) as JOUR, PAS_HEUREDEB, monthname(PAS_DATE),
	case
		when dayname(PAS_DATE) = 'Friday' AND PAS_HEUREDEB >= '19:00:00' THEN "Correct"
		when ((dayname(PAS_DATE) = 'Saturday' OR dayname(PAS_DATE) = 'Sunday') AND (PAS_HEUREDEB BETWEEN '10:00:00' AND '12:00:00')) THEN "Correct"
		when Date_format(PAS_DATE, '%m/%d') BETWEEN '06/15' AND '09/15' THEN "Correct"
        else 'Incorrect'
        end as Resultat
        from passage;

-- OR AVEC Fonction

DELIMITER $$
-- Drop function if exists ControleDate;
Create Function ControleDate (nomRencontre varchar(100))
returns VARCHAR (10) deterministic

BEGIN
        
        IF(
			(SELECT dayname(PAS_DATE) from passage p JOIN rencontre r ON r.REN_ID = p.REN_ID
            where REN_NOM = nomRencontre) = 'Friday' AND 
            (Select PAS_HEUREDEB from passage p JOIN rencontre r ON r.REN_ID = p.REN_ID 
            where REN_NOM = nomRencontre) >= '19:00:00') OR 
            
            (((SELECT dayname(PAS_DATE) from passage p JOIN rencontre r ON r.REN_ID = p.REN_ID 
            where REN_NOM = nomRencontre) = 'Saturday' OR 'Sunday') AND 
            (Select PAS_HEUREDEB from passage p JOIN rencontre r ON r.REN_ID = p.REN_ID 
            where REN_NOM = nomRencontre) BETWEEN '10:00:00' AND '12:00:00') OR 
            
            (SELECT Date_format(PAS_DATE, '%m-%d') from passage p JOIN rencontre r ON r.REN_ID = p.REN_ID 
            where REN_NOM = nomRencontre) BETWEEN (06-15) AND (09-15) THEN RETURN 'Correct';
            ELSE RETURN ('Incorrect');
     
        END IF;
       
END ; $$
DELIMITER ;

select ControleDate('VOULSTOCK');


--  Mettez en place les triggers liées à la suppression d'un groupe et à la suppression d'une oeuvre:

Create table Groupes_Supprimes (Groupe_ID int, Personne_ID int, Groupe_Nom varchar (150));
set Foreign_Key_Checks = 0;
DELIMITER $$
	Drop TRIGGER if exists Supprimer_Groupe;
	Create TRIGGER Supprimer_Groupe
    BEFORE DELETE ON groupe
    FOR EACH ROW
    BEGIN
		Insert INTO groupes_supprimes
		Value (old.GRO_ID, old.PER_ID, old.GRO_NOM);

    
    END$$
DELIMITER ;

DELETE FROM groupe
	WHERE GRO_ID = 11;
    
set Foreign_Key_Checks = 1;


-- Suppression d'un oeuvre:

set Foreign_Key_Checks = 0;
DELIMITER $$
	Drop TRIGGER if exists Supprimer_Oeuvre;
	Create TRIGGER Supprimer_Oeuvre
    AFTER DELETE ON chanson
    FOR EACH ROW
    BEGIN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Suppression n'est pas possible!!!";
    END$$
DELIMITER ;

DELETE FROM chanson
	WHERE CHA_ID = 5;
    
set Foreign_Key_Checks = 1;


-- Créez une procédure stockée qui sélectionne les groupes qui ne participent pas à une rencontre donnée, 
-- puis une autre qui renvoie le dernier numéro de rencontre insérée.

DELIMITER $$
Drop PROCEDURE if exists Non_Participant;
Create PROCEDURE Non_Participant(IN rencontre_donnee varchar (50))

BEGIN

	SELECT GRO_ID, GRO_NOM
    From groupe where GRO_ID not in (Select GRO_ID from passage p JOIN rencontre r ON p.REN_ID = r.REN_ID 
		where r.REN_NOM = rencontre_donnee);
    
END $$

DELIMITER ;

call Non_Participant('LES MUZIKELLES');

-- le dernier numéro de rencontre insérée

DELIMITER $$
Drop PROCEDURE if exists Dernier_Rencontre $$
Create Procedure Dernier_Rencontre ()

BEGIN
	
   SELECT max(REN_ID), REN_NOM as Resultat from rencontre;

END $$
DELIMITER ;

call Dernier_Rencontre();
SELECT @Resultat as DernierRencontre;


-- Transaction

DELIMITER $$

Drop function if exists Controle_Date $$
Create Function Controle_Date (idrencontre int)
returns VARCHAR (10) deterministic

BEGIN
        
        IF(
			(SELECT dayname(REN_DATEDEBUT) from rencontre where REN_ID = idrencontre) = 'Friday' AND 
            (Select time(REN_DATEDEBUT)from rencontre where REN_ID = idrencontre) >= '19:00:00') OR 
            
            (((SELECT dayname(REN_DATEDEBUT) from rencontre where REN_ID = idrencontre) = 'Saturday' OR 'Sunday') AND 
            (Select time(REN_DATEDEBUT) from rencontre where REN_ID = idrencontre) BETWEEN '10:00:00' AND '12:00:00') OR 
            
            (SELECT Date_format(REN_DATEDEBUT, '%m-%d') from rencontre where REN_ID = idrencontre) BETWEEN (06-15) AND (09-15) THEN RETURN 'Correct';
            ELSE RETURN ('Incorrect');
     
        END IF;
       
END ; $$
DELIMITER ;

select Controle_Date(10);



set autocommit = 0;
set foreign_key_checks = 0;

START TRANSACTION;

DELIMITER ||

Drop PROCEDURE if exists Nouveau_Rencontre ||
Create Procedure Nouveau_Rencontre(IN id_Personne int, IN id_Periodicite int, IN id_Region int, IN nomRencontre VARCHAR(100), lieuRencontre VARCHAR(100), dateDebut DATETIME, dateFin DATETIME, nPerRencontre VARCHAR(20))

BEGIN
    
    DECLARE Counter INT;
    DECLARE idrencontre int;
    
    set autocommit = 0;
	set foreign_key_checks = 0;

	START TRANSACTION;
    
    SET Counter = 0;
    WHILE Counter = 7 DO
		INSERT INTO rencontre(per_id, peri_id, reg_id, ren_nom, ren_lieu, ren_datedebut, ren_datefin, ren_nbpers)
		VALUES(id_Personne, id_Periodicite, id_Region, nomRencontre, lieuRencontre, dateDebut, dateFin, nPerRencontre);
        
        SET dateDebut = (Select adddate('REN_DATEDEBUT', INTERVAL 1 DAY) from rencontre where REN_ID = (Select max(REN_ID) from rencontre));
        SET dateFin = (SELECT adddate('REN_DATEFIN', INTERVAL 1 DAY) FROM rencontre WHERE REN_ID = (SELECT max (REN_ID) FROM rencontre));
        SET Counter = Counter + 1;
	
    END WHILE;
    
    SET idrencontre = (SELECT max(REN_ID) FROM rencontre);
    WHILE idrencontre > (SELECT max(REN_ID) FROM rencontre) - 7 DO
		IF ControleDate(idrencontre) LIKE 'Incorrect' THEN ROLLBACK;
        END IF;
        SET idrencontre = idrencontre - 1;
	END WHILE;
    COMMIT;
END ||
DELIMITER ;





